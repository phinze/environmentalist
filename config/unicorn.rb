# Spawn workers 
worker_processes 3

# Restart any workers that haven't responded in 30 seconds
timeout 30

# Listen on a Unix data socket
listen File.join(ENV['RAILS_ROOT'], 'tmp', 'pids', 'unicorn.sock'), :backlog => 2048

# Create the pid file
pid File.join(ENV['RAILS_ROOT'], 'tmp', 'pids', 'unicorn.pid')
