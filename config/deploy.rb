set :stages, %w(vagrant production)
require 'capistrano/ext/multistage'

require "bundler/capistrano"

set :rvm_type, :system
set :rvm_ruby_string,  'ruby-1.9.3-p194@environmentalist'

require "rvm/capistrano"

set :keep_releases, 1
after "deploy:update", "deploy:cleanup" 
