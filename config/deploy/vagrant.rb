set :rails_env, stage

if ENV['LOCAL_DEPLOY']
  set :scm, :none
  set :repository, "."
  set :deploy_via, :copy
  set :copy_exclude, ['.git', 'iso']
else
  set :scm, :git
  set :repository, "https://bitbucket.org/phinze/environmentalist/"
end

set :application, "environmentalist"
set :deploy_to, "/srv/#{application}"

vagrant_ssh_config = %x(vagrant ssh-config).split("\n")
vagrant_host = vagrant_ssh_config.grep(/HostName/).first.split(' ').last
vagrant_port = vagrant_ssh_config.grep(/Port/).first.split(' ').last
vagrant_key = vagrant_ssh_config.grep(/IdentityFile/).first.split(' ').last

set :use_sudo, false
set :normalize_asset_timestamps, false

ssh_options[:keys] = [vagrant_key]
set :user, "vagrant"
set :port, vagrant_port
role :web, vagrant_host
role :db, vagrant_host, :primary => true

BP = 'bundle exec bluepill --no-privileged -c ./tmp/pids/bluepill -l ./log/bluepill.log'

namespace :deploy do
  # Invoked during initial deployment
  task :start do
    run "cd #{current_path} && RAILS_ROOT='#{current_path}' RACK_ENV=#{rails_env} #{BP} load ./#{application}.pill"
  end

  # Invoked after each deployment afterwards
  task :restart do
    ignore_failure { run "cd #{current_path} && #{BP} stop unicorn && sleep 1 && #{BP} quit && sleep 1" }
    run "cd #{current_path} && RAILS_ROOT='#{current_path}' RACK_ENV=#{rails_env} #{BP} load ./#{application}.pill"
  end
end

namespace :rvm do
  desc 'Trust rvmrc file'
  task :trust_rvmrc do
    run "rvm rvmrc trust #{current_path}"
  end
end

after "deploy:update_code", "rvm:trust_rvmrc"


##
# Shorthand for ignoring exceptions
def ignore_failure(&block)
  begin
    yield
  rescue
  end
end
