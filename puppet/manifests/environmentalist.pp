stage { 'predeps': before => Stage['main'] }
class predeps {
  user { 'puppet': ensure => present }
  group { 'puppet': ensure => present }
}
class { 'predeps': stage => predeps }

node default {
  include rvm
  $ruby_version = 'ruby-1.9.3-p194'

  rvm::system_user { 'vagrant': }
  rvm_system_ruby { $ruby_version: ensure => present }
  Rvm_gem { ruby_version => $ruby_version }

  rvm_gemset { 'environmentalist': ruby_version => $ruby_version, ensure => present } 
  rvm_gem { 'bundler': ensure => latest }

  # include bluepill
  # include unicorn

  group { 'deploy': ensure => present }
  user { 'environmentalist':
    ensure => present,
    groups => ['deploy']
  }
  unixgroup::membership { 'vagrant-in-deploy':
    username => 'vagrant',
    group => 'deploy'
  }
  capistrano::application { 'environmentalist':
    owner => 'environmentalist', group => 'deploy'
  }

  include augeas
  include apt::backports

  file { '/data':
    ensure => directory,
    owner => 'root', group => 'root', mode => '0755'
  }

  $postgresql_data_dir = '/data/postgresql'

  include postgresql::debian::v9-1
  package { 'postgresql-server-dev-9.1': ensure => present }
  postgresql::database { 'environmentalist': }
  postgresql::user { 'environmentalist':
    superuser => true,
    password => 'environmentalist'
  }
  Class['apt::update'] -> Class['postgresql::base']

  include nginx
  nginx::resource::vhost { 'environmentalist.local':
    ensure   => present,
    proxy => 'http://unix:/srv/environmentalist/current/tmp/pids/unicorn.sock'
  }


}

Exec {
  path => ['/usr/local/bin', '/usr/bin', '/usr/sbin', '/sbin', '/bin']
}
