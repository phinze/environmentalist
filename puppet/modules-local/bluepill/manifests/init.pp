class bluepill {
  rvm_gem { 'bluepill':
    ensure => latest,
    ruby_version => 'system'
  }

  file { '/etc/bluepill':
    ensure => directory
    owner => 'root', group => 'root', mode => '0755'
  }
}
