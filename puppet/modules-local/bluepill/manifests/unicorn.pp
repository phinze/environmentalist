define bluepill::application(
  $application_name = $title
  $application_path
) {
  file { "/etc/bluepill/${application_name}.conf.rb":
    content => template("bluepill/unicorn.conf.rb.erb")
    owner => 'root', group => 'root', mode => '0644'
  }
}
