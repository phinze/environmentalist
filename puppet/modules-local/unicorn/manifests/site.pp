define unicorn::site(
  $application_name = $title,
  $application_path = "/srv/${title}",
  $use_bluepill = true
) {
  file { "/etc/unicorn/${application_name}.conf.rb":
    content => template("unicorn/unicorn.conf.rb.erb")
    owner => 'root', group => 'root', mode => '0644'
  }

  if $use_bluepill == true { 
    bluepill::unicorn { $application_name:
      application_path => $application_path
    }
  }
}
