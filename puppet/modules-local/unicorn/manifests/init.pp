class unicorn(
  $rack_version = "1.4.1"
){
  rvm_gem { "rack": ensure => $rack_version }
  rvm_gem { "unicorn": ensure => "4.1.1", require => Rvm_gem["rack"] }

  file { '/etc/unicorn':
    ensure => directory
    owner => 'root', group => 'root', mode => '0755'
  }
}
