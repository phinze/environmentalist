define capistrano::application(
  $application_name = $title,
  $root_path = '/srv',
  $owner,
  $group
){
  file { [
    "${root_path}/${application_name}",
    "${root_path}/${application_name}/shared",
    "${root_path}/${application_name}/shared/log",
    "${root_path}/${application_name}/shared/pids",
    "${root_path}/${application_name}/shared/system",
    "${root_path}/${application_name}/releases"
  ]:
    ensure => directory,
    mode => 775, owner => $owner, group => $group
  }
}
