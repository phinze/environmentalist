define unixgroup::membership(
  $username,
  $group
) {
  exec { "/usr/sbin/usermod -a -G $group $username":
    unless  => "/bin/cat /etc/group | grep $group | grep $username",
    require => [User[$username], Group[$group]];
  }
}
